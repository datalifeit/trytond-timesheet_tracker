# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import company
from . import timesheet


def register():
    Pool.register(
        company.Employee,
        company.Company,
        timesheet.TimesheetLineClockProcessStart,
        timesheet.Timesheet,
        company.EmployeeDailyJournalPrintStart,
        module='timesheet_tracker', type_='model')
    Pool.register(
        timesheet.TimesheetLineClockProcess,
        company.EmployeeDailyJournalPrint,
        module='timesheet_tracker', type_='wizard')
    Pool.register(
        company.EmployeeDailyJournal,
        module='timesheet_tracker', type_='report')
