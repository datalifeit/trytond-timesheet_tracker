# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.model import fields, ModelView, dualmethod
from datetime import datetime, timedelta
from trytond.modules.company import CompanyReport
from trytond.wizard import Wizard, StateView, Button, StateReport
import calendar
from dateutil.relativedelta import relativedelta
from dateutil.tz import tz as dateutil_tz
from trytond.pyson import Eval, Id
from trytond.rpc import RPC
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    work = fields.Many2One('timesheet.work', 'Work')


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    work = fields.Many2One('timesheet.work', 'Work')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()
        cls._buttons.update({
            'clock_process': {}
        })
        cls.__rpc__.update({
            'is_working': RPC(readonly=False, instantiate=0),
            'delete_lines': RPC(readonly=False, instantiate=0)
        })

    @property
    def work_used(self):
        work = self.work or self.company.work
        if not work:
            # Allow empty value on on_change
            if Transaction().readonly:
                work = None
            else:
                raise UserError(gettext(
                    'timesheet_tracker.msg_company_employee_missing_work',
                    employee=self.rec_name,
                    company=self.company.rec_name))
        return work

    @classmethod
    def delete_lines(cls, records, data={}):
        pool = Pool()
        Line = pool.get('timesheet.line')

        lines = [line for record in records
            for line in Line.search(
                record._get_timesheet_line_domain(**data)[:-1])]
        Line.delete(lines)

    @classmethod
    def is_working(cls, records, data={}):
        pool = Pool()
        Line = pool.get('timesheet.line')
        res = {}
        for record in records:
            lines = Line.search(record._get_timesheet_line_domain(**data))
            res[record.id] = len(lines) != 0
        return res

    @dualmethod
    @ModelView.button
    def clock_process(cls, records, data={}):
        pool = Pool()
        Line = pool.get('timesheet.line')

        today = datetime.now()
        for record in records:
            lines = Line.search(record._get_timesheet_line_domain(**data))
            if lines:
                line = lines[0]
                line.end_time = today.replace(second=0,
                    microsecond=0)
                line.on_change_end_time()
            else:
                line = record._get_timesheet_line(**data)
                line.on_change_start_time()
            line.save()

    def _get_timesheet_line_domain(self, **kwargs):
        today = datetime.now()
        if kwargs:
            kwargs['date'] = today.date()
            kwargs['end_time'] = None
            return [(k, '=', v) for k, v in kwargs.items()]
        return [
            ('date', '=', today.date()),
            ('employee', '=', self),
            ('work', '=', self.work_used),
            ('end_time', '=', None)
        ]

    def _get_timesheet_line(self, **kwargs):
        Line = Pool().get('timesheet.line')
        today = datetime.now()
        if kwargs:
            kwargs['start_time'] = today.replace(second=0, microsecond=0)
            return Line(**kwargs)
        return Line(
            employee=self,
            work=self.work_used,
            start_time=today.replace(second=0, microsecond=0),
            date=today.date())


class EmployeeDailyJournalPrintStart(ModelView):
    '''Employee Daily Journal Print start'''
    __name__ = 'company.employee.daily_journal.print.start'

    employee = fields.Many2One('company.employee', 'Employee',
        states={
            'readonly': ~Id('timesheet', 'group_timesheet_admin').in_(
                Eval('context', {}).get('groups', []))
        })
    month = fields.Selection([
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
        ], 'Month', required=True, sort=False)
    year = fields.Integer('Year', required=True)

    @staticmethod
    def default_employee():
        User = Pool().get('res.user')
        user = User(Transaction().user)

        return user.employee.id if user.employee else None

    @staticmethod
    def default_month():
        return str(datetime.now().month)

    @staticmethod
    def default_year():
        return datetime.now().year


class EmployeeDailyJournalPrint(Wizard):
    '''Employee Daily Journal Print'''
    __name__ = 'company.employee.daily_journal.print'

    start = StateView('company.employee.daily_journal.print.start',
        'timesheet_tracker.daily_journal_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True)])
    print_ = StateReport('company.employee.daily_journal')

    def do_print_(self, action):
        pool = Pool()
        Line = pool.get('timesheet.line')
        line = Line.__table__()

        date = datetime(year=self.start.year,
            month=int(self.start.month), day=1)
        data = {
            'year': self.start.year,
            'month': int(self.start.month),
            'ids': [self.start.employee.id] if self.start.employee else None
        }

        if not data['ids']:
            cursor = Transaction().connection.cursor()
            cursor.execute(*line.select(line.employee,
                where=(
                    (line.company == Transaction().context.get('company')) &
                    (line.date >= date) &
                    (line.date < date + relativedelta(months=1))),
                group_by=line.employee)
            )
            data['ids'] = [x[0] for x in cursor.fetchall()]
        return action, data


class EmployeeDailyJournal(CompanyReport):
    '''Employee Daily Journal'''
    __name__ = 'company.employee.daily_journal'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(EmployeeDailyJournal,
            cls).get_context(records, header, data)
        pool = Pool()
        Line = pool.get('timesheet.line')

        cal = calendar.Calendar()
        employee_workdays = {}
        for employee in records:
            workdays = []
            date = datetime(year=data['year'], month=data['month'], day=1)

            lines = Line.search([
                ('employee', '=', employee.id),
                ('date', '>=', date),
                ('date', '<', date + relativedelta(months=1))
            ], order=[('date', 'ASC'), ('start_time', 'ASC')])
            date = date.date()
            report_context['date'] = date.strftime('%m/%Y')
            monthly_total_hours = timedelta(hours=0)
            for day in cal.itermonthdays(data['year'], data['month']):
                if not day:
                    continue
                date = date.replace(day=day)
                day_lines = [line for line in lines if line.date == date]
                len_lines = len(day_lines)
                total_hours = cls._compute_total_hours(day_lines)
                monthly_total_hours += total_hours
                workday = {
                    'day': day,
                    'in_morning': cls._format_date(
                        day_lines[0].start_time) if day_lines else '',
                    'in_afternoon': cls._format_date(
                        day_lines[0].end_time) if day_lines else '',
                    'out_morning': cls._format_date(
                        day_lines[1].start_time) if len_lines == 2 else '',
                    'out_afternoon': cls._format_date(
                        day_lines[1].end_time) if len_lines == 2 else '',
                    'total_hours': cls._format_timedelta(total_hours)
                }
                workdays.append(workday)
            employee_workdays[employee.id] = workdays
        report_context['employee_workdays'] = employee_workdays
        report_context['monthly_total_hours'] = \
            cls._format_timedelta(monthly_total_hours)
        return report_context

    @classmethod
    def _format_date(cls, date):
        pool = Pool()
        Company = pool.get('company.company')

        if not date:
            return ''
        if not Transaction().context.get('company', None):
            return date
        company = Company(Transaction().context['company'])
        lzone = (dateutil_tz.gettz(company.timezone)
            if company.timezone else dateutil_tz.tzutc())
        szone = dateutil_tz.tzutc()
        date = date.replace(
            tzinfo=szone).astimezone(lzone).replace(tzinfo=None)
        return date.strftime('%H:%M')

    @classmethod
    def _compute_total_hours(cls, day_lines):
        total_hours = timedelta(hours=0)
        for day_line in day_lines:
            total_hours += day_line.duration
        return total_hours

    @classmethod
    def _format_timedelta(cls, time_):
        return '{0:02d}:{1:02d}'.format(
            int(time_.total_seconds() // 3600),
            int(time_.total_seconds() % 3600 // 60))
