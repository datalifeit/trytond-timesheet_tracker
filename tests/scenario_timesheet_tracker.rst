==========================
Timesheet Tracker Scenario
==========================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from datetime import datetime, timedelta
    >>> today = datetime.now()
    >>> import time

Install timesheet_tracker::

    >>> config = activate_modules('timesheet_tracker')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create user and add employee::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> my_user = User()
    >>> my_user.name = 'User'
    >>> my_user.login = 'user'
    >>> my_user.company = company
    >>> timesheet_group, = Group.find([('name', '=', 'Timesheet Administration')])
    >>> my_user.groups.append(timesheet_group)
    >>> employee_party = Party(name="Employee")
    >>> employee_party.save()
    >>> employee = Employee(party=employee_party)
    >>> employee.save()
    >>> my_user.employees.append(employee)
    >>> my_user.employee = employee
    >>> my_user.save()
    >>> admin = User(1)
    >>> set_user(my_user)

Missing employee error on wizard Clock Process::

    >>> clock_process = Wizard('timesheet.line.clock_process', [])
    >>> clock_process.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('There is no employee defined on the user "User"', ''))

Wizard Clock Process::

    >>> clock_process.execute('process') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('There is no work defined neither on the employee "Employee" nor on the company "Dunder Mifflin"', ''))
    >>> Work = Model.get('timesheet.work')
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> set_user(admin)
    >>> employee.work = work
    >>> employee.save()
    >>> set_user(my_user)
    >>> clock_process.execute('process')

Check Timesheet line values::

    >>> Line = Model.get('timesheet.line')
    >>> line, = Line.find([])
    >>> line.employee == employee
    True
    >>> line.work == work
    True
    >>> line.date == today.date()
    True
    >>> not line.end_time
    True
    >>> line.start_time == datetime.now().replace(second=0, microsecond=0)
    True

Clock process again after 1 minute::

    >>> time.sleep(60)
    >>> clock_process = Wizard('timesheet.line.clock_process', [])
    >>> clock_process.execute('process')
    >>> line.reload()
    >>> line.end_time == line.start_time + timedelta(minutes=1)
    True
    >>> clock_process = Wizard('timesheet.line.clock_process', [])
    >>> clock_process.execute('process')
    >>> lines = Line.find([])
    >>> len(lines)
    2
    >>> line2, = [l for l in lines if not l.end_time]
    >>> line2.start_time == datetime.now().replace(second=0, microsecond=0)
    True

Print report::

    >>> daily_journal = Wizard('company.employee.daily_journal.print', [])
    >>> daily_journal.form.month == str(datetime.now().month)
    True
    >>> daily_journal.execute('print_')

