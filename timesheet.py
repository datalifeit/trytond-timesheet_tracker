# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from trytond.model import ModelView
from trytond.exceptions import UserError
from trytond.i18n import gettext


class TimesheetLineClockProcessStart(ModelView):
    """Timesheet Line Clock Process Start"""
    __name__ = 'timesheet.line.clock_process.start'


class TimesheetLineClockProcess(Wizard):
    """Timesheet Line Clock Process"""
    __name__ = 'timesheet.line.clock_process'

    start = StateView('timesheet.line.clock_process.start',
        'timesheet_tracker.clock_process_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'process', 'tryton-ok', True),
        ])
    process = StateTransition()

    def transition_process(self):
        User = Pool().get('res.user')
        user = User(Transaction().user)

        employee = user.employee
        if not employee:
            raise UserError(gettext(
                'timesheet_tracker.'
                'msg_timesheet_line_clock_process_missing_employee',
                user=user.rec_name))
        employee.clock_process()
        return 'end'


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    @classmethod
    def __setup__(cls):
        super(Timesheet, cls).__setup__()
        cls.end_time.states['required'] = None
